FROM golang:1.15 as builder
MAINTAINER Alban Moreon bambinito.dev@gmail.com

WORKDIR /build

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o gitlab-publisher .

FROM alpine:latest

RUN addgroup -g 998 appuser && \
	adduser -S -h /app -u 998 appuser

COPY --from=builder /build/gitlab-publisher /app

WORKDIR /app
RUN chown appuser:appuser -R /app
USER appuser

ENTRYPOINT ["/app/gitlab-publisher"]
