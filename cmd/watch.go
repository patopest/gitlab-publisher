package cmd

import (
	"os"
	"fmt"
	"time"
	"gitlab-publisher/gitlab"

	"github.com/spf13/cobra"
)

var pipelineId string
var namespace string
var projectId string
var sa_token string
var gitlab_token string
var task_name string

var watchCmd = &cobra.Command{
	Use: "watch",
	Short: "Watch the tasks of the Tekton pipeline and publish their status to Gitlab",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
        return InitConfig(cmd)
    },
	Run: func(cmd *cobra.Command, args []string) {
    	watch()
  	},
}

func init() {
	rootCmd.AddCommand(watchCmd)

	watchCmd.Flags().StringVarP(&pipelineId, "pipelineId", "i", "", "The tekton pipeline Id")
	watchCmd.MarkFlagRequired("pipelineId")
	watchCmd.Flags().StringVarP(&namespace, "namespace", "n", "default", "The namespace in which the pipeline is running")
	watchCmd.Flags().StringVarP(&commit, "commit", "c", "", "The commit id")
	watchCmd.MarkFlagRequired("commit")
	watchCmd.Flags().StringVarP(&projectId, "projectId", "p", "", "The Gitlab API project id of the repo")
	watchCmd.MarkFlagRequired("projectId")
	watchCmd.Flags().StringVarP(&sa_token, "sa-token", "", "", "The Kubernetes service account token")
	watchCmd.MarkFlagRequired("sa-token")
	watchCmd.Flags().StringVarP(&gitlab_token, "gitlab-token", "", "", "The Gitlab API token to use")
	watchCmd.MarkFlagRequired("gitlab-token")
	watchCmd.Flags().StringVarP(&task_name, "task-name", "t", "publish", "The Tekton task name for this app")
}

var tasks []gitlab.Task

func watch() {
	newTasks := gitlab.Watch(pipelineId, namespace, sa_token)
	//fmt.Println(newTasks)

	for i := 0; i < len(newTasks); i++{
		task := newTasks[i]
		if task.Name == task_name {
			continue
		}
		if i < len(tasks) {
			if task.Name == tasks[i].Name && task.Reason == tasks[i].Reason{
				continue
			}
		}
		gitlab.PostCommitStatus(projectId, commit, task.Name, task.Reason, task.Message, gitlab_token)
	}

	SUCCESSFUL := 0
	for i := 0; i < len(newTasks); i++ {
		task:= newTasks[i]
		if task.Reason == "Failed" {
			fmt.Printf("Task %s failed, shutting down...\n", task.Name)
			os.Exit(0)
		}
		if task.Reason == "Succeeded" {
			SUCCESSFUL += 1
		}
	}
	if SUCCESSFUL >= len(newTasks)-1 {
		fmt.Println("Pipeline ended, shutting down...")
		os.Exit(0)
	}

	tasks = newTasks
	time.Sleep(15 * time.Second)
	watch()
}