package cmd

import (
	"github.com/spf13/cobra"

	"gitlab-publisher/gitlab"
)

var repo string
//var commit string
var name string
var status string
var description string
var token string

var publishCmd = &cobra.Command{
	Use: "publish",
	Short: "Publish the status of the CI pipeline on the triggered commit",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		return InitConfig(cmd)
	},
	Run: func(cmd *cobra.Command, args []string) {
		publish()
	},
}

func init() {
	rootCmd.AddCommand(publishCmd)
	
	publishCmd.Flags().StringVarP(&repo, "repo", "r", "", "The repository url")
	publishCmd.MarkFlagRequired("repo")
	publishCmd.Flags().StringVarP(&commit, "commit", "c", "", "The commit id")
	publishCmd.MarkFlagRequired("commit")
	publishCmd.Flags().StringVarP(&name, "name", "n", "", "The gitlab CI job name")
	publishCmd.Flags().StringVarP(&status, "status", "s", "", "The status to publish")
	publishCmd.MarkFlagRequired("status")
	publishCmd.Flags().StringVarP(&description, "description", "d", "", "The gitlab CI job description")
	publishCmd.Flags().StringVarP(&token, "token", "t", "", "The gitlab API token to use")
	publishCmd.MarkFlagRequired("token")
}

func publish() {
	gitlab.Publish(repo, commit, name, status, description, token)
}
