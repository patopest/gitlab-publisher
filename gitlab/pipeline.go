package gitlab

import (
	"os"
	"fmt"
	"net/http"
	"crypto/tls"
	"io/ioutil"

	"github.com/buger/jsonparser"
)


type Task struct {
	Name 		string
	StartTime 	string
	Status 		string
	Reason 		string
	Message 	string
	TaskRun 	string
}


func Watch(name, namespace, token string) []Task {

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	tasks := getTasks(name, namespace, token)

	return tasks
}

func getTasks(name, namespace, token string) []Task {
	url := makeUrl(namespace, "pipelineruns", name)

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Bearer " + token)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to make request to %s : %v\n", url, err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	//fmt.Println(string(body))

	cond, _, _, err := jsonparser.Get(body, "status", "conditions", "[0]")
	if err != nil {
		fmt.Println("Error whilst parsing json: ", err)
		return make([]Task, 0)
	}

	fmt.Println("Pipeline conditions:  ",string(cond))

	var tasks []Task

	paths := [][]string{
		[]string{"pipelineTaskName"},
		[]string{"status", "startTime"},
		[]string{"status", "conditions", "[0]", "status"},
		[]string{"status", "conditions", "[0]", "reason"},
		[]string{"status", "conditions", "[0]", "message"},
	}

	jsonparser.ObjectEach(body, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
        var task Task
        task.TaskRun = string(key)
        jsonparser.EachKey(value, func(idx int, value []byte, vt jsonparser.ValueType, err error){
			switch idx {
				case 0:
					task.Name = string(value)
				case 1:
					task.StartTime = string(value)
				case 2:
					task.Status = string(value)
				case 3:
					task.Reason = string(value)
				case 4:
					task.Message = string(value)
			}
		}, paths...)
		tasks = append(tasks, task)
		return nil
	}, "status", "taskRuns")

	for i, j := 0, len(tasks)-1; i < j; i, j = i+1, j-1 {
        tasks[i], tasks[j] = tasks[j], tasks[i]
    }

    fmt.Print("Found the following TaskRuns on the pipeline: ")
    for i := 0; i < len(tasks); i++ {
    	fmt.Printf("%s, ", tasks[i].TaskRun)
    }
    fmt.Print("\n")


    return tasks
}

func makeUrl(namespace, resource, name string) string {
	base := "https://" + os.Getenv("KUBERNETES_SERVICE_HOST") + ":" + os.Getenv("KUBERNETES_SERVICE_PORT")
	base += "/apis/tekton.dev/v1beta1/namespaces/"

	base += namespace

	base += "/" + resource

	if name != "" {
		base += "/" + name
	}

	return base
}

/*
func getAuth() string{
	file, err := ioutil.ReadFile("/var/run/secrets/token")
	if err != nil {
        fmt.Println("Error reading token file: ", err)
    }

    return string(file)
}
*/
