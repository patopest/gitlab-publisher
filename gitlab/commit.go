package gitlab

import (
	"os"
	"fmt"
	"strings"
	"net/url"
	"net/http"
	"io/ioutil"
	"github.com/buger/jsonparser"	
)

var base = "https://gitlab.com/api/v4"
var client = &http.Client{}

func Publish(repo, commit, name, status, description, token string) {

	userId := getUserId(token)
	projectId := getProjectId(userId, repo, token)

	PostCommitStatus(projectId, commit, name, status, description, token)
}


func getUserId(token string) string {
	url := base + "/user"
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Bearer " + token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to make request to %s : %v\n", url, err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	userId, _, _, err := jsonparser.Get(body, "id")
	if err != nil {
		fmt.Println("Error whilst parsing json: ", err)
		os.Exit(1)
	}

	fmt.Printf("Found user with id: %s\n", string(userId))
	return string(userId)
}


func getProjectId(user, repo, token string) string {
	url := base + "/users/" + user + "/projects"
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Bearer " + token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to make request to %s : %v\n", url, err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	type Project struct {
		Id 	string
		Url string
	}

	paths := [][]string{
		[]string{"id"},
		[]string{"web_url"},
	}

	var project Project

	jsonparser.ArrayEach(body, func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
        //fmt.Printf("Value: '%s' Type: %s\n", string(value), dataType)
        var tmp Project
        jsonparser.EachKey(value, func(idx int, value []byte, vt jsonparser.ValueType, err error){
			switch idx {
				case 0:
					tmp.Id = string(value)
				case 1:
					tmp.Url = string(value)
			}
		}, paths...)

		if tmp.Url == repo {
			project = tmp
		}
		return
	})

	fmt.Printf("Found project with id: %s\n", project.Id)
	return project.Id
}

type BuildStateValue string

// These constants represent all valid build states.
const (
	Pending  BuildStateValue = "pending"
	Created  BuildStateValue = "created"
	Running  BuildStateValue = "running"
	Success  BuildStateValue = "success"
	Failed   BuildStateValue = "failed"
	Canceled BuildStateValue = "canceled"
	Skipped  BuildStateValue = "skipped"
	Manual   BuildStateValue = "manual"
)

func getBuildStateValue(v string) BuildStateValue {
	var state BuildStateValue
	switch v {
		case "Pending", "pending":
			state = Pending 
		case "created":
			state = Created
		case "Running", "running":
			state = Running
		case "Succeeded", "success":
			state = Success
		case "Failed", "failed":
			state = Failed
		case "TaskRunCancelled", "cancelled":
			state = Canceled

	}
	return state
}


func PostCommitStatus(project, commit, name, status, description, token string){
	v := url.Values{}
	url := base + "/projects/" + project + "/statuses/" + commit
	if name != "" {
		v.Set("name", name)
	}
	v.Set("state", string(getBuildStateValue(status)))
	
	if description != "" {
		if len(description) >= 260 {
			description = strings.Split(description, "(")[0]
		}
		v.Set("description", description)
	}

	url += "?" + v.Encode()
	//fmt.Println(url)

	req, err := http.NewRequest("POST", url, nil)
	req.Header.Set("Authorization", "Bearer " + token)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("Failed to make request to %s : %v\n", url, err)
		return

	}
	defer resp.Body.Close()

	if resp.StatusCode != 201 {
		body,_ := ioutil.ReadAll(resp.Body)
		fmt.Printf("Request returned %d: %s\n", resp.StatusCode, string(body))
		return
	}

	fmt.Printf("Successful request to post commit status for %s\n", name)
}

