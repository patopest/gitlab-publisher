package main

import (
  "gitlab-publisher/cmd"
)

func main() {
  cmd.Execute()
}